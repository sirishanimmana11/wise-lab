import tkinter as tk
from tkinter import messagebox
import mysql.connector
import pandas as pd
import plotly.graph_objects as go
import os
def insert_data():
    year = int(year_entry.get())
    month = int(month_entry.get())
    value = float(value_entry.get())
    units = units_entry.get()
    
    try:
        connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        cursor = connection.cursor()

        # Check if the month already exists for the given year
        query_check = "SELECT COUNT(*) FROM deta WHERE year = %s AND month = %s"
        cursor.execute(query_check, (year, month))
        count = cursor.fetchone()[0]
        if count > 0:
            messagebox.showinfo("Error", "Data for the given month and year already exists. Please update instead.")
            return

        query = "INSERT INTO deta (year, month, value, units) VALUES (%s, %s, %s, %s)"
        cursor.execute(query, (year, month, value, units))
        connection.commit()
        messagebox.showinfo("Success", "Data inserted successfully.")

    except mysql.connector.Error as err:
        messagebox.showerror("Error", f"An error occurred: {err}")

    finally:
        if 'cursor' in locals():
            cursor.close()
        if 'connection' in locals():
            connection.close()

def fetch_data():
    try:
        connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        cursor = connection.cursor()

        query = "SELECT * FROM deta"
        cursor.execute(query)

        rows = cursor.fetchall()
        data = {
            'Year': [],
            'Month': [],
            'Data': [],
            'Units': []
        }

        for row in rows:
            data['Year'].append(row[0])
            data['Month'].append(row[1])
            data['Data'].append(row[2])
            data['Units'].append(row[3])

        df = pd.DataFrame(data)
        display_text.insert(tk.END, df.to_string() + "\n")

    except mysql.connector.Error as err:
        messagebox.showerror("Error", f"An error occurred: {err}")

    finally:
        if 'cursor' in locals():
            cursor.close()
        if 'connection' in locals():
            connection.close()
def modify_data():
    try:
        connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        cursor = connection.cursor()

        year_value = year_entry.get()
        month_value = month_entry.get()
        value_value = value_entry.get()
        units_value = units_entry.get()

        # Input validation
        if not year_value or not month_value or not value_value or not units_value:
            messagebox.showerror("Error", "Please fill in all fields.")
            return

        year = int(year_value)
        month = int(month_value)
        value = float(value_value)

        # Check if the data exists for the given month and year
        query_check = "SELECT COUNT(*) FROM deta WHERE year = %s AND month = %s"
        cursor.execute(query_check, (year, month))
        count = cursor.fetchone()[0]
        if count == 0:
            messagebox.showinfo("Error", "Data for the given month and year does not exist.")
            return

        # Update the data
        query_update = "UPDATE deta SET value = %s, units = %s WHERE year = %s AND month = %s"
        cursor.execute(query_update, (value, units_value, year, month))
        connection.commit()
        messagebox.showinfo("Success", "Data modified successfully.")

    except ValueError:
        messagebox.showerror("Error", "Please enter valid numeric values for year, month, and value.")

    except mysql.connector.Error as err:
        messagebox.showerror("Error", f"An error occurred: {err}")

    finally:
        if 'cursor' in locals():
            cursor.close()
        if 'connection' in locals():
            connection.close()
def delete_data():
    try:
        connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        cursor = connection.cursor()

        # Fetch all data from the database
        query_fetch = "SELECT * FROM deta"
        cursor.execute(query_fetch)

        rows = cursor.fetchall()
        data = {
            'Year': [],
            'Month': [],
            'Data': [],
            'Units': []
        }

        # Prepare data for displaying in a DataFrame
        for row in rows:
            data['Year'].append(row[0])
            data['Month'].append(row[1])
            data['Data'].append(row[2])
            data['Units'].append(row[3])

        df = pd.DataFrame(data)

        # Display the data in tabular form
        display_text.insert(tk.END, df.to_string() + "\n")

        # Prompt the user to select a row for deletion
        row_number = int(input("Enter the row number to delete: "))

        # Check if the selected row number is valid
        if row_number < 1 or row_number > len(df):
            messagebox.showerror("Error", "Invalid row number.")
            return

        # Get the year and month for the selected row
        year = df.iloc[row_number - 1]['Year']
        month = df.iloc[row_number - 1]['Month']

        # Delete the selected row from the database
        query_delete = "DELETE FROM deta WHERE year = %s AND month = %s"
        cursor.execute(query_delete, (year, month))
        connection.commit()
        messagebox.showinfo("Success", "Row deleted successfully.")

    except mysql.connector.Error as err:
        messagebox.showerror("Error", f"An error occurred: {err}")

    finally:
        if 'cursor' in locals():
            cursor.close()
        if 'connection' in locals():
            connection.close()
def visualize_data():
    try:
        connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        cursor = connection.cursor()

        query = "SELECT * FROM deta"
        cursor.execute(query)

        rows = cursor.fetchall()
        data = {
            'Year': [],
            'Month': [],
            'Data': [],
            'Units': []
        }

        for row in rows:
            data['Year'].append(row[0])
            data['Month'].append(row[1])
            data['Data'].append(row[2])
            data['Units'].append(row[3])

        df = pd.DataFrame(data)
        
        # Convert boolean values to integers (True -> 1, False -> 0)
        df['Data'] = df['Data'].astype(int)

        fig = go.Figure()

        years = df['Year'].unique()
        buttons = []
        for year in years:
            mask = df['Year'] == year
            fig.add_trace(go.Bar(x=[month_abbr[m] for m in df['Month'][mask]], y=df['Data'][mask], name=f"{year} - {df['Units'][mask].iloc[0]}"))
            buttons.append(dict(
                label=str(year),
                method="update",
                args=[{"visible": [year == y for y in years]}, {"title": f"Monthly Data for {year}"}]
            ))

            # Find maximum value and corresponding month for each year
            max_value = df[df['Year'] == year]['Data'].max()
            max_month = df[(df['Year'] == year) & (df['Data'] == max_value)]['Month'].values[0]
            fig.add_annotation(
                x=max_month,
                y=max_value,
                text=f'Max: {max_value} ({month_abbr[max_month]})',
                showarrow=True,
                arrowhead=1
            )

        fig.update_layout(
            updatemenus=[
                {
                    "buttons": buttons,
                    "direction": "down",
                    "showactive": True,
                    "x": 0.1,
                    "xanchor": "left",
                    "y": 1.1,
                    "yanchor": "top"
                }
            ],
            xaxis_title="Month",
            yaxis_title=f"Data ({df['Units'].iloc[0]})",
            title="Monthly Data Analysis",
            showlegend=True
        )

        # Save the plot as an image
        image_path = "plot.png"
        fig.write_image(image_path)

        # Create a new window to display the plot
        plot_window = tk.Toplevel(root)
        plot_window.title("Visualize Data")

        # Display the image in the window
        image = tk.PhotoImage(file=image_path)
        label = tk.Label(plot_window, image=image)
        label.image = image
        label.pack()

        # Remove the image file after displaying
        os.remove(image_path)

    except mysql.connector.Error as err:
        messagebox.showerror("Error", f"An error occurred: {err}")

    finally:
        if 'cursor' in locals():
            cursor.close()
        if 'connection' in locals():
            connection.close()
def delete_data():
    try:
        connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        cursor = connection.cursor()

        year_value = year_entry.get()
        month_value = month_entry.get()

        # Input validation
        if not year_value or not month_value:
            messagebox.showerror("Error", "Please fill in both Year and Month fields.")
            return

        year = int(year_value)
        month = int(month_value)

        # Check if the data exists for the given month and year
        query_check = "SELECT COUNT(*) FROM deta WHERE year = %s AND month = %s"
        cursor.execute(query_check, (year, month))
        count = cursor.fetchone()[0]
        if count == 0:
            messagebox.showinfo("Error", "Data for the given month and year does not exist.")
            return

        # Delete the selected row
        query_delete = "DELETE FROM deta WHERE year = %s AND month = %s"
        cursor.execute(query_delete, (year, month))
        connection.commit()
        messagebox.showinfo("Success", "Data deleted successfully.")

    except ValueError:
        messagebox.showerror("Error", "Please enter valid numeric values for year and month.")

    except mysql.connector.Error as err:
        messagebox.showerror("Error", f"An error occurred: {err}")

    finally:
        if 'cursor' in locals():
            cursor.close()
        if 'connection' in locals():
            connection.close()
def on_submit():
    choice = choice_var.get()
    if choice == 1:
        insert_data()
    elif choice == 2:
        fetch_data()
    elif choice == 3:
        visualize_data()
    elif choice == 4:
        modify_data()
    elif choice == 5:
        fetch_data()
        delete_data()
    elif choice == 6:
        root.quit()
    else:
        messagebox.showerror("Error", "Invalid choice. Please enter a valid option.")

host = "localhost"
user = "root "
password = "password"
database = "sys"

month_abbr = {
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dec'
}

root = tk.Tk()
root.title("Data Management")

choice_var = tk.IntVar()

tk.Label(root, text="Select an option:").pack()

options = [
    ("Insert Data", 1),
    ("View Data", 2),
    ("Visualize Data", 3),
    ("Modify Data", 4),
    ("Delete Data", 5),
    ("Exit", 6)
]

for text, value in options:
    tk.Radiobutton(root, text=text, variable=choice_var, value=value).pack(anchor=tk.W)

submit_button = tk.Button(root, text="Submit", command=on_submit)
submit_button.pack()

year_label = tk.Label(root, text="Year:")
year_label.pack()
year_entry = tk.Entry(root)
year_entry.pack()

month_label = tk.Label(root, text="Month (1-12):")
month_label.pack()
month_entry = tk.Entry(root)
month_entry.pack()

value_label = tk.Label(root, text="Value:")
value_label.pack()
value_entry = tk.Entry(root)
value_entry.pack()

units_label = tk.Label(root, text="Units:")
units_label.pack()
units_entry = tk.Entry(root)
units_entry.pack()

display_text = tk.Text(root)
display_text.pack()

root.mainloop()